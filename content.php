<article class="blog-post<?php if (is_single()) {echo ' single';} ?>">
  <?php if (is_single() === false) : ?>
    <a class="permalink" href="<?php the_permalink()?>"></a>
  <?php endif ?>

  <!-- Feature image -->
  <?php
  if( has_post_thumbnail() ):
      echo get_the_post_thumbnail();
  endif;
  ?>

  <div class="blog-post-container">
    <!-- Title -->
    <h2 class="blog-post-title"><?php the_title(); ?></h2>

    <?php if (is_single()) : ?>
    <!-- Content -->
    <div class="blog-post-content">
      <?php the_content(); ?>
    </div>

    <?php else : ?>

    <!-- Excerpt -->
    <div class="blog-post-excerpt">
      <?php the_excerpt(); ?>
    </div>

    <?php endif ?>

    <!-- Date and author -->
    <p class="blog-post-meta"><?php the_date(); ?> par <?php the_author_posts_link(); ?></p>
  </div>

</article>
